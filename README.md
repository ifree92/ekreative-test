# Decisioner

**Requirements: Docker**

### Production

```bash
$ docker-compose up
```

Then go to http://localhost:5588

### Development

#### Run backend development

```bash
$ ./server-dev.sh
```

Inside the container:

```bash
$ npm i # install all npm dependencies
$ npm run watch # run server in watch debug mode on 5588 port
```

#### Run frontend development

```bash
$ ./client-dev.sh
```

Inside the container:

```bash
$ npm i # install all npm dependencies
$ npm start # start webpack dev server on 4200 port

```

Deploy new frontend version to `server/public`:
```bash
$ npm run deploy
```

Link to frontend dev server: http://localhost:9080
