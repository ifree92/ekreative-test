import {TestBed, async} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {DecisionerModule} from "../decisioner/decisioner.module";

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports: [
        DecisionerModule
      ]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have 'app-decisioner' tag`, async() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeNode;
    expect(compiled.querySelector('app-decisioner')).not.toBe(null);
  });
});
