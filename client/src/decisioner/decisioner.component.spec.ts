import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DecisionerComponent} from './decisioner.component';
import {
  MatButtonModule,
  MatDialogModule, MatExpansionModule,
  MatIconModule,
  MatListModule, MatMenuModule,
  MatProgressBarModule, MatProgressSpinnerModule,
  MatSnackBarModule, MatToolbarModule
} from "@angular/material";
import {FileUploadModule} from "ng2-file-upload";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {BrowserModule} from "@angular/platform-browser";
import {UploadDialogComponent} from "./upload-dialog/upload-dialog.component";
import {HttpClientModule} from "@angular/common/http";
import {ToolbarComponent} from "./toolbar/toolbar.component";
import {ProductsComponent} from "./products/products.component";
import {ProductComponent} from "./product/product.component";
import {ApiService} from "../services/api.service";

describe('DecisionerComponent', () => {
  let component: DecisionerComponent;
  let fixture: ComponentFixture<DecisionerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        DecisionerComponent,
        ProductComponent,
        ProductsComponent,
        ToolbarComponent,
        UploadDialogComponent,
      ],
      imports: [
        BrowserModule,
        BrowserAnimationsModule,
        MatToolbarModule,
        MatButtonModule,
        MatMenuModule,
        MatIconModule,
        HttpClientModule,
        MatExpansionModule,
        MatListModule,
        MatProgressSpinnerModule,
        MatDialogModule,
        FileUploadModule,
        MatProgressBarModule,
        MatSnackBarModule
      ],
      providers: [ApiService]
    })
      .compileComponents();
  }));

  it('should create', () => {
    const fixture = TestBed.createComponent(DecisionerComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have 'app-toolbar'`, () => {
    const fixture = TestBed.createComponent(DecisionerComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeNode;
    expect(compiled.querySelector('app-toolbar')).not.toBe(null);
  });

  it(`should have 'app-products'`, () => {
    const fixture = TestBed.createComponent(DecisionerComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeNode;
    expect(compiled.querySelector('app-products')).not.toBe(null);
  });

});
