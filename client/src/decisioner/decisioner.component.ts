import {Component, OnInit} from '@angular/core';
import {ApiService} from "../services/api.service";
import {MatDialog, MatSnackBar} from "@angular/material";
import {UploadDialogComponent} from "./upload-dialog/upload-dialog.component";

@Component({
  selector: 'app-decisioner',
  templateUrl: './decisioner.component.html',
  styleUrls: ['./decisioner.component.css']
})
export class DecisionerComponent implements OnInit {

  products: IProductItem[] = [];
  showSpinner = false;

  constructor(private apiService: ApiService, private dialog: MatDialog, private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.refreshProducts();
  }

  /**
   * Get products from server and set it in local component's collection
   */
  refreshProducts() {
    this.showSpinner = true;
    this.apiService.getProducts().subscribe((data) => {
      this.showSpinner = false;
      if (data.result) {
        this.products = data.data.sort((a, b) => a.idp > b.idp ? 1 : -1);
        this.showSnackBarMessage(`Received ${this.products.length} products`);
      } else {
        if (data.message) {
          this.showSnackBarMessage(data.message);
        } else {
          this.showSnackBarMessage('Unable to get products');
        }
      }
    });
  }

  /**
   * Open upload dialog modal window and subscribe on events
   */
  openUploadDialog() {
    const dialogRef = this.dialog.open(UploadDialogComponent, {disableClose: true});
    // dialogRef.afterClosed().subscribe(() => this.refreshProducts());
    dialogRef.componentInstance.onError.subscribe((err) => this.showSnackBarMessage(`Error: ${err}`));
    dialogRef.componentInstance.onSuccess.subscribe(() => this.showSnackBarMessage('DB is synchronising... Please refresh list'));
  }

  /**
   * Show snackbar message
   * @param {string} message
   */
  private showSnackBarMessage(message: string) {
    this.snackBar.open(message, null, {duration: 4000});
  }
}
