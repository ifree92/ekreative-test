import {NgModule} from "@angular/core";
import {
  MatButtonModule,
  MatDialogModule, MatExpansionModule,
  MatIconModule,
  MatListModule, MatMenuModule,
  MatProgressBarModule, MatProgressSpinnerModule,
  MatSnackBarModule, MatToolbarModule
} from "@angular/material";
import {FileUploadModule} from "ng2-file-upload";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {BrowserModule} from "@angular/platform-browser";
import {DecisionerComponent} from "./decisioner.component";
import {UploadDialogComponent} from "./upload-dialog/upload-dialog.component";
import {HttpClientModule} from "@angular/common/http";
import {ToolbarComponent} from "./toolbar/toolbar.component";
import {ProductsComponent} from "./products/products.component";
import {ProductComponent} from "./product/product.component";
import {ApiService} from "../services/api.service";

@NgModule({
  declarations: [
    DecisionerComponent,
    ProductComponent,
    ProductsComponent,
    ToolbarComponent,
    UploadDialogComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatMenuModule,
    MatIconModule,
    HttpClientModule,
    MatExpansionModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    FileUploadModule,
    MatProgressBarModule,
    MatSnackBarModule
  ],
  providers: [ApiService],
  entryComponents: [UploadDialogComponent],
  exports: [DecisionerComponent]
})

export class DecisionerModule {
}
