import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  @Input() title: string = '';
  @Input() id: number = 0;
  @Input() names: string[] = [];

  constructor() {

  }

  ngOnInit() {

  }

}
