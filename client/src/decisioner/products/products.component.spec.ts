import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProductsComponent} from './products.component';
import {
  MatButtonModule,
  MatDialogModule, MatExpansionModule,
  MatIconModule,
  MatListModule, MatMenuModule,
  MatProgressBarModule, MatProgressSpinnerModule,
  MatSnackBarModule, MatToolbarModule
} from "@angular/material";
import {FileUploadModule} from "ng2-file-upload";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {BrowserModule} from "@angular/platform-browser";
import {DecisionerComponent} from "../decisioner.component";
import {UploadDialogComponent} from "../upload-dialog/upload-dialog.component";
import {HttpClientModule} from "@angular/common/http";
import {ToolbarComponent} from "../toolbar/toolbar.component";
import {ProductComponent} from "../product/product.component";
import {ApiService} from "../../services/api.service";

describe('ProductsComponent', () => {
  let component: ProductsComponent;
  let fixture: ComponentFixture<ProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        DecisionerComponent,
        ProductComponent,
        ProductsComponent,
        ToolbarComponent,
        UploadDialogComponent,
      ],
      imports: [
        BrowserModule,
        BrowserAnimationsModule,
        MatToolbarModule,
        MatButtonModule,
        MatMenuModule,
        MatIconModule,
        HttpClientModule,
        MatExpansionModule,
        MatListModule,
        MatProgressSpinnerModule,
        MatDialogModule,
        FileUploadModule,
        MatProgressBarModule,
        MatSnackBarModule
      ],
      providers: [ApiService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
