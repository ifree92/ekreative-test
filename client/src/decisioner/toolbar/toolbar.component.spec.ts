import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ToolbarComponent} from './toolbar.component';
import {
  MatButtonModule,
  MatDialogModule, MatExpansionModule,
  MatIconModule,
  MatListModule, MatMenuModule,
  MatProgressBarModule, MatProgressSpinnerModule,
  MatSnackBarModule, MatToolbarModule
} from "@angular/material";
import {FileUploadModule} from "ng2-file-upload";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {BrowserModule} from "@angular/platform-browser";
import {DecisionerComponent} from "../decisioner.component";
import {UploadDialogComponent} from "../upload-dialog/upload-dialog.component";
import {AppComponent} from "../../app/app.component";
import {HttpClientModule} from "@angular/common/http";
import {ProductsComponent} from "../products/products.component";
import {ProductComponent} from "../product/product.component";
import {ApiService} from "../../services/api.service";

describe('ToolbarComponent', () => {
  let component: ToolbarComponent;
  let fixture: ComponentFixture<ToolbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        DecisionerComponent,
        ProductComponent,
        ProductsComponent,
        ToolbarComponent,
        UploadDialogComponent,
      ],
      imports: [
        BrowserModule,
        BrowserAnimationsModule,
        MatToolbarModule,
        MatButtonModule,
        MatMenuModule,
        MatIconModule,
        HttpClientModule,
        MatExpansionModule,
        MatListModule,
        MatProgressSpinnerModule,
        MatDialogModule,
        FileUploadModule,
        MatProgressBarModule,
        MatSnackBarModule
      ],
      providers: [ApiService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
