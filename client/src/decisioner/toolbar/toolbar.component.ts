import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent {

  @Output() menuRefresh: EventEmitter<any> = new EventEmitter();
  @Output() menuUpload: EventEmitter<any> = new EventEmitter();

  constructor() {
  }

  /**
   * Handling click on refresh menu item
   */
  menuRefreshHandler() {
    this.menuRefresh.emit();
  }

  /**
   * Handling click on upload menu item
   */
  menuUploadHandler() {
    this.menuUpload.emit();
  }
}
