import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {MatDialogRef} from "@angular/material";
import {FileUploader} from 'ng2-file-upload';
import {api} from '../../environments/environment';

@Component({
  selector: 'app-upload-dialog',
  templateUrl: './upload-dialog.component.html',
  styleUrls: ['./upload-dialog.component.css']
})
export class UploadDialogComponent implements OnInit {

  public uploader:FileUploader = new FileUploader({url: `http://${api.host}:${api.port}/api/v1/products/parse`});
  public progressUploading = 0;
  public uploadingProcess = false;

  @Output() onSuccess: EventEmitter<any> = new EventEmitter();
  @Output() onError: EventEmitter<string> = new EventEmitter();

  constructor(private matDialogRef: MatDialogRef<UploadDialogComponent>) { }

  ngOnInit() {
    // Prevent CORS errors during debug process
    this.uploader.onAfterAddingFile = (item) => {
      item.withCredentials = false;
    }
  }

  /**
   * Handle click on Close button
   */
  onCloseHandler() {
    this.matDialogRef.close();
  }

  /**
   * Handle click on Upload button
   */
  onUploadHandler() {
    this.uploadingProcess = true;
    const file = this.uploader.queue[0];
    file.onProgress = (progress) => this.onProgressFileUpload(progress);
    file.onError = (err) => this.onErrorFileUpload(err);
    file.onSuccess = () => this.onSuccessFileUpload();
    file.upload();
  }

  /**
   * Internal MatDialog event
   * on error file uploaded
   * @param err
   */
  private onErrorFileUpload(err: string) {
    this.uploadingProcess = false;
    this.onCloseHandler();
    try {
      const jsonError = JSON.parse(err);
      this.onError.emit(jsonError.message);
    } catch (e) {
      this.onError.emit(err);
    }
  }

  /**
   * Internal MatDialog event
   * on success file uploaded
   */
  private onSuccessFileUpload() {
    this.uploadingProcess = false;
    this.onCloseHandler();
    this.onSuccess.emit();
  }

  /**
   * Internal MatDialog event
   * on progress change
   * @param {number} progress
   */
  private onProgressFileUpload(progress) {
    this.progressUploading = progress;
  }
}
