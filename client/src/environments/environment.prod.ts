export const environment = {
  production: true
};

export const api = {
  host: 'localhost',
  port: 5588
};
