import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";

import {api} from '../environments/environment';
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) {
  }

  /**
   * Get actual API URL
   * @returns {string}
   */
  private static getUrl(): string {
    return `http://${api.host}:${api.port}/api/v1`;
  }

  /**
   * Get list products from server
   * @returns {Observable<IGetProductsResponse>}
   */
  getProducts(): Observable<IGetProductsResponse> {
    return this.http.get<IGetProductsResponse>(`${ApiService.getUrl()}/products`);
  }
}
