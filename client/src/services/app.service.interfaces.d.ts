interface IProductItem {
  idp: number;
  title: string;
  names: string[];
}

interface IGetProductsResponse {
  result: boolean;
  data: IProductItem[],
  message?: string;
}
