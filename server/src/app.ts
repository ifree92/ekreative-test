import express from 'express';
import config, {isDev} from './config';
import logger from './services/logger';
import {getVersion} from './helpers/helpers';
import {error} from './helpers/responses';
import router from './router';
import fileUpload from 'express-fileupload';
import mongoose from 'mongoose';
import path from 'path';
import cors from 'cors';

const log = logger(module);
log.info(`init ${getVersion()}`);

/**
 * Async entry point of application
 * @returns {Promise<void>}
 */
async function main() {

  await mongoose.connect(`mongodb://${config().mongodb.host}:${config().mongodb.port}/${config().mongodb.database}`);

  const app = express();

  app.use(express.static(path.join(__dirname, '..', 'public')));

  if (isDev()) {
    app.use(cors());
  }

  app.use(fileUpload({
    limits: { fileSize: 20 * 1024 * 1024 },
    abortOnLimit: true
  }));

  app.use((req, res, next) => {
    log.info(req.method, req.url);
    next();
  });

  app.use('/', router);

  app.use((req, res) => {
    res.status(404).json(error('Not found'));
  });

  app.listen(config().server.port, () => {
    log.info(`Listening on ${config().server.port}`);
  });
}

main()
  .then(() => log.info('Initialized'))
  .catch((err) => {
    log.error(err);
    process.exit(1);
  });

process.on('SIGTERM', () => {
  log.info('SIGTERM OBTAINED');
  process.exit(0);
});
