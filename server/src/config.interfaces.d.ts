interface IConfigServer {
  port: number;
}

interface IConfigLogs {
  path: string;
  maxFiles: number;
}

interface IConfigMongo {
  host: string;
  port: number;
  database: string;
}

interface IConfigFile {
  server: IConfigServer;
  logs: IConfigLogs;
  mongodb: IConfigMongo;
}

declare module '*config.production.json' {
  const value: IConfigFile;
  export default value;
}

declare module '*config.development.json' {
  const value: IConfigFile;
  export default value;
}