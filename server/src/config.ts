import configDev from '../config.development.json';
import configProd from '../config.production.json';

let isDevFlag = false;

/**
 * Returns needed config depends on production or development environment
 * @returns {IConfigFile}
 */
export default function getConfig(): IConfigFile {
  if (isDev()) {
    return configDev;
  }
  return configProd;
}

/**
 * Check is --dev argument set
 * @returns {boolean}
 */
export function isDev(): boolean {
  return isDevFlag;
}

/**
 * Check and init start flags
 */
function init() {
  isDevFlag = process.argv.indexOf('--dev') >= 0;
  if (isDevFlag) console.log('--dev flag enabled');
}

init();
