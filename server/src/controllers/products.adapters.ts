/**
 * Try parse obtained file as JSON array of products
 * @param productsJsonText
 * @returns {IParsedProductItem[]}
 */
export function parseProducts(productsJsonText: string): IParsedProductItem[] {
  const parsedProducts = JSON.parse(productsJsonText);
  const products: IParsedProductItem[] = [];
  if (parsedProducts && Array.isArray(parsedProducts) && parsedProducts.length > 0) {
    for (let parsedProduct of parsedProducts) {
      if (parsedProduct
        && typeof parsedProduct === 'object'
        && 'id' in parsedProduct
        && 'names' in parsedProduct
        && typeof parsedProduct.id === 'number'
        && Array.isArray(parsedProduct.names)
      ) {
        for (let nameItem of parsedProduct.names) {
          if (typeof nameItem !== 'string') throw new Error('Bad format');
        }
        products.push({
          id: parsedProduct.id,
          names: parsedProduct.names
        });
      }
    }
    return products;
  }
  throw new Error('Bad format');
}
