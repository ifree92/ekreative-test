import {Request, Response} from 'express';
import {success, error} from '../helpers/responses';
import logger from '../services/logger';
import {get} from '../storages/Products';
import {UploadedFile} from 'express-fileupload';
import {parseProducts} from './products.adapters';
import {update} from '../services/productsExecutor';

const log = logger(module);

/**
 * Controller for GET /api/v1/products
 * @param {Request} req
 * @param {Response} res
 */
export function getProducts(req: Request, res: Response) {
  let limit = 100;
  let skip = 0;
  if (req.query && typeof req.query === 'object') {
    if ('limit' in req.query && typeof req.query.limit === 'number' && req.query.limit > 0) {
      limit = req.query.limit;
    }
    if ('skip' in req.query && typeof req.query.skip === 'number' && req.query.skip > 0) {
      skip = req.query.skip;
    }
  }
  get(limit, skip)
    .then((products) => res.json(success<IProductTitleItem[]>(products)))
    .catch((err) => res.status(500).json(error(err.message)));
}

/**
 * Controller for POST /api/v1/products/parse
 * Handling upload file via <input type="file" name="file"/>
 * @param {Request} req
 * @param {Response} res
 */
export function postProductsParse(req: Request, res: Response) {
  if (req.files && typeof req.files === 'object' && 'file' in req.files) {
    // @ts-ignore
    const file:UploadedFile = req.files.file;
    let products: IParsedProductItem[] = [];
    try {
      products = parseProducts(file.data.toString());
      res.json(success<string>('OK'));
    } catch (e) {
      res.status(400).json(error(e.message));
    }
    update(products);
  } else {
    res.status(400).json(error('Bad request. The "file" field expected'));
  }
}
