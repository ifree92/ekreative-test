import {readFileSync} from 'fs';
import {resolve} from 'path';

/**
 * Get current application version
 * @returns {string}
 */
export function getVersion(): string {
  let file: any;
  try {
    file = JSON.parse(readFileSync(resolve(__dirname, '..', '..', 'package.json')).toString().trim());
  } catch (e) {
  }
  if (typeof file === 'object' && 'version' in file && typeof file.version === 'string') {
    return file.version;
  }
  return '0.0.0';
}
