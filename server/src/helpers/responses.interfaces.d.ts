interface IResponseSuccess<T> {
  readonly result: boolean;
  readonly data: T;
}

interface IResponseError {
  readonly result: boolean;
  readonly message: string;
}
