/**
 * Returns the object prepared for answer for API
 * @param data
 * @returns {IResponseSuccess}
 */
export function success<T>(data: T): IResponseSuccess<T> {
  return { result: true, data };
}

/**
 * Returns the object with description of error for API
 * @param message
 * @returns {IResponseError}
 */
export function error(message: string): IResponseError {
  return { result: false, message };
}
