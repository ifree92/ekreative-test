interface IProductItem {
  idp: number;
  names: string[];
  title: string;
}