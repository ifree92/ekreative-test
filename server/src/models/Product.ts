import {Document, model, Model} from 'mongoose';
import ProductItemSchema from '../schemas/ProductItem';

export interface IProductModel extends IProductItem, Document {

}

const Product: Model<IProductModel> = model<IProductModel>('Product', ProductItemSchema);

export default Product;
