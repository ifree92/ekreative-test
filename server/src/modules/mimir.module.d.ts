declare namespace __mimir {
  interface Itokenize {
    (line: string): string[];
  }

  interface Itfidf {
    (word: string, text: string, lines: string[]): number;
  }

  interface Imimir {
    tokenize: Itokenize;
    tfidf: Itfidf;
  }
}

declare module 'mimir' {
  const _module: __mimir.Imimir;
  export default _module;
}
