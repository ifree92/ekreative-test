import express from 'express';
import {
  getProducts,
  postProductsParse
} from './controllers/products';

const router = express.Router();

router.get('/api/v1/products', getProducts);
router.post('/api/v1/products/parse', postProductsParse);

export default router;
