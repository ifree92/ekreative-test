import mongoose from 'mongoose';

const ProductItem = new mongoose.Schema({
  idp: {
    unique: true,
    type: mongoose.SchemaTypes.Number
  },
  names: [mongoose.SchemaTypes.String],
  title: mongoose.SchemaTypes.String
});

export default ProductItem;
