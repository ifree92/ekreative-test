import * as chai from 'chai';
import {describe, it} from 'mocha';

import {
  fixFirstWord,
  groupAttributes,
  clearAttributes,
  getPossibleAttributes,
  toTitleCase,
  calculateTfIdf
} from './Decisioner.helpers';

describe('Decisioner.helpers', () => {

  describe('fixFirstWord()', () => {
    it('hello world -> Hello world', () => {
      chai.assert.equal(fixFirstWord('hello world'), 'Hello world');
    });
    it('empty string', () => {
      chai.assert.equal(fixFirstWord(''), '');
    });
    it('one word', () => {
      chai.assert.equal(fixFirstWord('hello'), 'Hello');
    });
    it('digits', () => {
      chai.assert.equal(fixFirstWord('23123 hello'), '23123 hello');
    });
  });

  describe('groupAttributes()', () => {
    it('empty attributes', () => {
      chai.assert.deepEqual(groupAttributes([]), []);
    });
    it('several items', () => {
      chai.assert.deepEqual(groupAttributes(['12 mm', '12 mm', '14 mm', '14 mm', '14 mm', '14 mm', '14 mm']), [
        {text: '14 mm', times: 5},
        {text: '12 mm', times: 2}
      ]);
    });
  });

  describe('clearAttributes()', () => {
    it('empty attributes', () => {
      chai.assert.deepEqual(clearAttributes([]), []);
    });
    it('several attributes', () => {
      chai.assert.deepEqual(clearAttributes(['12mm', '25  Sm', '33 PX']), ['12 mm', '25 sm', '33 px']);
    });
  });

  describe('getPossibleAttributes()', () => {
    it('empty text', () => {
      chai.assert.deepEqual(getPossibleAttributes([]), []);
    });
    it('parsing attributes', () => {
      chai.assert.deepEqual(getPossibleAttributes([
          'Dr. Brandt Lineless Lines No More For Lips 7.5g/0.25oz',
          'Vichy LiftActiv Night Global Anti-Wrinkle & Firming Care 50ml/1.69oz'
        ]),
        ['7.5g', '0.25oz', '50ml', '1.69oz']);
    });
  });

  describe('toTitleCase()', () => {
    it('hello world -> Hello World', () => {
      chai.assert.equal(toTitleCase('hello world'), 'Hello World');
    });
    it('empty string', () => {
      chai.assert.equal(toTitleCase(''), '');
    });
    it('digits', () => {
      chai.assert.equal(toTitleCase('2176378216378 hello 32137'), '2176378216378 Hello 32137');
    });
  });

  describe('calculateTfIdf()', () => {
    const set1 = ["B. Kamins Purifying Toner", "B. Kamins Purifying Toner-6 oz.", "B. Kamins Hydrogen-ion Moisturizing Toner oily skin 8.oz", "B. Kamins Chemist - Purifying Toner", "B Kamins Purifying Toner 6oz"];
    const result1 = [{"text":"B. Kamins Purifying Toner","totalScores":-0.13674116759546595,"scores":[{"word":"b","score":-0.04558038919848865},{"word":"kamins","score":-0.04558038919848865},{"word":"purifying","score":0},{"word":"toner","score":-0.04558038919848865}]},{"text":"B. Kamins Chemist - Purifying Toner","totalScores":0.07386521229845827,"scores":[{"word":"b","score":-0.03646431135879092},{"word":"kamins","score":-0.03646431135879092},{"word":"chemist","score":0.18325814637483104},{"word":"purifying","score":0},{"word":"toner","score":-0.03646431135879092}]},{"text":"B Kamins Purifying Toner 6oz","totalScores":0.07386521229845827,"scores":[{"word":"b","score":-0.03646431135879092},{"word":"kamins","score":-0.03646431135879092},{"word":"purifying","score":0},{"word":"toner","score":-0.03646431135879092},{"word":"6oz","score":0.18325814637483104}]},{"text":"B. Kamins Purifying Toner-6 oz.","totalScores":0.12573595503689744,"scores":[{"word":"b","score":-0.026045936684850654},{"word":"kamins","score":-0.026045936684850654},{"word":"purifying","score":0},{"word":"toner","score":-0.026045936684850654},{"word":"6","score":0.13089867598202215},{"word":"oz","score":0.07297508910942724}]},{"text":"B. Kamins Hydrogen-ion Moisturizing Toner oily skin 8.oz","totalScores":0.5461605344629058,"scores":[{"word":"b","score":-0.01823215567939546},{"word":"kamins","score":-0.01823215567939546},{"word":"hydrogen","score":0.09162907318741552},{"word":"ion","score":0.09162907318741552},{"word":"moisturizing","score":0.09162907318741552},{"word":"toner","score":-0.01823215567939546},{"word":"oily","score":0.09162907318741552},{"word":"skin","score":0.09162907318741552},{"word":"8","score":0.09162907318741552},{"word":"oz","score":0.051082562376599076}]}];
    const set2 = ["Vichy Purete Thermale Perfecting Toner 200ml", "Vichy Purete Thermale Perfecting Toner (200ml)", "Vichy Purete Thermale Perfecting Toner - For Sensitive Skin 200ml/6.7oz", "Vichy Laboratoires Purete Thermale Perfecting Toner", "Purete Thermale Perfecting Toner", "Vichy Purete Thermal Refreshing Toner", "Vichy Laboratoires Purete Thermale Perfecting Toner", "6.76 oz", "Vichy Purete Thermale Perfecting Toner 6.76 oz", "Vichy Purete Thermale Perfecting Toner 200ml Genuine &", "Vichy Laboratories Purete Thermale Perfecting Toner 6.8 oz", "Vichy Laboratories Purete Thermale Perfecting Toner 6.8 Oz"];
    const result2 = [{"text":"Purete Thermale Perfecting Toner","totalScores":0.04350568849481484,"scores":[{"word":"purete","score":0},{"word":"thermale","score":0.02175284424740742},{"word":"perfecting","score":0.02175284424740742},{"word":"toner","score":0}]},{"text":"Vichy Purete Thermale Perfecting Toner (200ml)","totalScores":0.16235755261754126,"scores":[{"word":"vichy","score":0.01243019671280424},{"word":"purete","score":0},{"word":"thermale","score":0.01243019671280424},{"word":"perfecting","score":0.01243019671280424},{"word":"toner","score":0},{"word":"200ml","score":0.12506696247912855}]},{"text":"Vichy Purete Thermale Perfecting Toner 200ml","totalScores":0.1894171447204648,"scores":[{"word":"vichy","score":0.01450189616493828},{"word":"purete","score":0},{"word":"thermale","score":0.01450189616493828},{"word":"perfecting","score":0.01450189616493828},{"word":"toner","score":0},{"word":"200ml","score":0.14591145622564997}]},{"text":"Vichy Laboratoires Purete Thermale Perfecting Toner","totalScores":0.2745547486814633,"scores":[{"word":"vichy","score":0.01450189616493828},{"word":"laboratoires","score":0.23104906018664842},{"word":"purete","score":0},{"word":"thermale","score":0.01450189616493828},{"word":"perfecting","score":0.01450189616493828},{"word":"toner","score":0}]},{"text":"Vichy Laboratoires Purete Thermale Perfecting Toner","totalScores":0.2745547486814633,"scores":[{"word":"vichy","score":0.01450189616493828},{"word":"laboratoires","score":0.23104906018664842},{"word":"purete","score":0},{"word":"thermale","score":0.01450189616493828},{"word":"perfecting","score":0.01450189616493828},{"word":"toner","score":0}]},{"text":"Vichy Purete Thermale Perfecting Toner 200ml Genuine &","totalScores":0.3660327921938555,"scores":[{"word":"vichy","score":0.01087642212370371},{"word":"purete","score":0},{"word":"thermale","score":0.01087642212370371},{"word":"perfecting","score":0.01087642212370371},{"word":"toner","score":0},{"word":"200ml","score":0.10943359216923748},{"word":"genuine","score":0.22396993365350687}]},{"text":"Vichy Purete Thermale Perfecting Toner 6.76 oz","totalScores":0.4019930512503281,"scores":[{"word":"vichy","score":0.01087642212370371},{"word":"purete","score":0},{"word":"thermale","score":0.01087642212370371},{"word":"perfecting","score":0.01087642212370371},{"word":"toner","score":0},{"word":"6","score":0.08664339756999316},{"word":"76","score":0.17328679513998632},{"word":"oz","score":0.10943359216923748}]},{"text":"Vichy Laboratories Purete Thermale Perfecting Toner 6.8 oz","totalScores":0.5113598634580574,"scores":[{"word":"vichy","score":0.00966793077662552},{"word":"laboratories","score":0.15403270679109896},{"word":"purete","score":0},{"word":"thermale","score":0.00966793077662552},{"word":"perfecting","score":0.00966793077662552},{"word":"toner","score":0},{"word":"6","score":0.07701635339554948},{"word":"8","score":0.15403270679109896},{"word":"oz","score":0.09727430415043331}]},{"text":"Vichy Laboratories Purete Thermale Perfecting Toner 6.8 Oz","totalScores":0.5113598634580574,"scores":[{"word":"vichy","score":0.00966793077662552},{"word":"laboratories","score":0.15403270679109896},{"word":"purete","score":0},{"word":"thermale","score":0.00966793077662552},{"word":"perfecting","score":0.00966793077662552},{"word":"toner","score":0},{"word":"6","score":0.07701635339554948},{"word":"8","score":0.15403270679109896},{"word":"oz","score":0.09727430415043331}]},{"text":"Vichy Purete Thermal Refreshing Toner","totalScores":0.7341060630891479,"scores":[{"word":"vichy","score":0.017402275397925937},{"word":"purete","score":0},{"word":"thermal","score":0.358351893845611},{"word":"refreshing","score":0.358351893845611},{"word":"toner","score":0}]},{"text":"Vichy Purete Thermale Perfecting Toner - For Sensitive Skin 200ml/6.7oz","totalScores":0.8178807205268139,"scores":[{"word":"vichy","score":0.007910125180875426},{"word":"purete","score":0},{"word":"thermale","score":0.007910125180875426},{"word":"perfecting","score":0.007910125180875426},{"word":"toner","score":0},{"word":"for","score":0.16288722447527773},{"word":"sensitive","score":0.16288722447527773},{"word":"skin","score":0.16288722447527773},{"word":"200ml","score":0.07958806703217272},{"word":"6","score":0.06301338005090412},{"word":"7oz","score":0.16288722447527773}]},{"text":"6.76 oz","totalScores":0.9849700930112453,"scores":[{"word":"6","score":0.23104906018664842},{"word":"76","score":0.46209812037329684},{"word":"oz","score":0.29182291245129993}]}];
    it ('set 1', () => {
      chai.assert.deepEqual(calculateTfIdf(set1), result1);
    });
    it('set 2', () => {
      chai.assert.deepEqual(calculateTfIdf(set2), result2);
    });
  });
});