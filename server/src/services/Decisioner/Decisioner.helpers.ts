import * as _ from 'lodash';
import mimir from 'mimir';

/**
 * Get analyzed text with calculated tfidf coefficient
 * @param {string[]} lines
 * @returns {ITextScores[]}
 */
export function calculateTfIdf(lines: string[]): ITextScores[] {
  const calculatedLines: ITextScores[] = [];
  lines.forEach((textLine) => {
    const scores: IWordScores[] = [];
    mimir.tokenize(textLine).forEach((word) => {
      if (!word) return;
      scores.push({ word, score: mimir.tfidf(word, textLine, lines) });
    });
    const sum = scores.reduce((acc, wordItem) => {
      acc += wordItem.score;
      return acc;
    }, 0);
    calculatedLines.push({
      text: textLine,
      totalScores: sum,
      scores
    });
  });
  return calculatedLines.sort((a, b) => a.totalScores > b.totalScores ? 1 : -1);
}

/**
 * Make it to lower case + add space between digit and description
 * @param attributes
 * @returns {string[]}
 */
export function clearAttributes(attributes: string[]): string[] {
  return attributes.map((item) => {
    const lowerCaseItem = item.toLowerCase();
    const matchedDigs = lowerCaseItem.match(/\d+(\.\d)*/);
    const matchedText = lowerCaseItem.match(/[a-z]+/);
    let dig = '';
    let text = '';
    if (matchedDigs) dig = matchedDigs[0];
    if (matchedText) text = matchedText[0];
    const finalTitle = `${dig} ${text}`;
    return finalTitle.trim();

  });
}

/**
 * Optimize / grouping attributes
 * @param attributes
 * @returns {IGrouppedAttribute[]}
 */
export function groupAttributes(attributes: string[]): IGrouppedAttribute[] {
  const filteredAttributes = clearAttributes(attributes);
  const grouppedAttributes: IGrouppedAttribute[] = [];

  filteredAttributes.forEach((attribute) => {
    const index = _.findIndex(grouppedAttributes, {text: attribute});
    if (index >= 0) {
      grouppedAttributes[index].times++;
    } else {
      grouppedAttributes.push({ text: attribute, times: 1 });
    }
  });

  grouppedAttributes.sort((a, b) => a.times < b.times ? 1 : -1);

  return grouppedAttributes;
}

/**
 * Get possible attributes
 * @param {string[]} lines
 * @returns {string[]}
 */
export function getPossibleAttributes(lines: string[]): string[] {
  const foundLines: string[] = [];
  const matchedLines = lines.join(' ').match(/\d+(\.\d+)*( *[a-zA-Z]+)?/gi);
  if (matchedLines) {
    matchedLines.forEach((item) => foundLines.push(item));
  }
  return foundLines;
}

/**
 * Get the string with first letter in each word with upper case
 * @param {string} text
 * @returns {string}
 */
export function toTitleCase(text: string): string {
  return text.replace(/\w\S*/g, (txt) => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase());
}

/**
 * Get the sentence with fixed title in first word
 * @param {string} text
 * @returns {string}
 */
export function fixFirstWord(text: string): string {
  let fixedStr = text.trim().split(' ');
  if (fixedStr.length > 0) {
    fixedStr[0] = toTitleCase(fixedStr[0]);
  }
  return fixedStr.join(' ');
}
