interface IGrouppedAttribute {
  text: string;
  times: number;
}

interface IWordScores {
  word: string;
  score: number;
}

interface ITextScores {
  text: string;
  totalScores: number;
  scores: IWordScores[];
}
