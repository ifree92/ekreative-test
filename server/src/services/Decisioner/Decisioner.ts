import {
  calculateTfIdf,
  groupAttributes,
  getPossibleAttributes,
  fixFirstWord
} from './Decisioner.helpers';

/**
 * Return the best title from proposed
 * @param {string[]} titles
 * @returns {string}
 */
export function solveBestTitle(titles: string[]): string {
  if (titles.length === 0) return '';
  const calculatedLines = calculateTfIdf(titles);
  let bestTitle = fixFirstWord(calculatedLines[0].text);
  if (getPossibleAttributes([bestTitle]).length === 0) {
    const attributes = groupAttributes(getPossibleAttributes(titles));
    if (attributes.length > 0) {
      bestTitle += ` ${attributes[0].text}`;
    }
  }
  return bestTitle;
}
