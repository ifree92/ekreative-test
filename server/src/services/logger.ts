import {Logger, transports, LoggerInstance} from 'winston';
import 'winston-daily-rotate-file';
import path from 'path';
import config from '../config';
import {getVersion} from '../helpers/helpers';

const version = getVersion();

/**
 * Create the logger for special module
 * @param {NodeModule} module
 * @returns {LoggerInstance}
 */
function logger(module: NodeModule): LoggerInstance {
  const moduleName = getModuleName(module);
  return new Logger({
    transports: [
      new transports.Console({
        level: 'silly',
        timestamp: true,
        label: `${moduleName}`,
        handleException: true,
        json: false,
        colorize: true
      }),
      new transports.DailyRotateFile({
        filename: 'counter_v5_app_',
        dirname: config().logs.path,
        datePattern: `${version}_yyyy-MM-ddTHH.log`,
        zippedArchive: false,
        maxFiles: config().logs.maxFiles,
        level: 'silly',
        json: false,
        label: `(h)${moduleName}`
      })
    ]
  })
}

// ================== HELPERS ==================

/**
 * Return the string view of module
 * @param {NodeModule} module
 * @returns {string}
 */
function getModuleName(module: NodeModule): string {
  return module.filename.split(path.sep).slice(-3).join(path.sep);
}

// Exports
export default logger;
