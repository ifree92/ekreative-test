import {solveBestTitle} from './Decisioner';
import {addOrUpdate} from '../storages/Products';
import logger from '../services/logger';

const log = logger(module);

const queue: IParsedProductItem[][] = [];
let busy = false;

/**
 * Start updating DB and "calculate" titles
 * @param {IParsedProductItem[]} products
 */
export function update(products: IParsedProductItem[]) {
  if (products.length > 0) queue.push(products);
  if (busy) return;
  busy = true;
  const startMs = Date.now();
  const productToExecute = queue.shift();
  if (productToExecute) {
    log.info(`Start execution ${productToExecute.length}`);
    process(productToExecute)
      .then(() => {
        log.info(`Executed ${Date.now() - startMs}ms`);
        busy = false;
        if (queue.length > 0) update([]);
      })
      .catch((err) => {
        log.error(err);
        busy = false;
        if (queue.length > 0) update([]);
      });
  } else {
    busy = false;
    if (queue.length > 0) update([]);
  }
}

/**
 * The main async process
 * @param {IParsedProductItem[]} products
 * @returns {Promise<void>}
 */
async function process(products: IParsedProductItem[]): Promise<void> {
  for (const product of products) {
    const title = solveBestTitle(product.names);
    await addOrUpdate({idp: product.id, names: product.names, title});
  }
}
