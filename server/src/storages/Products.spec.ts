import {assert} from 'chai';
import {describe, it, before} from 'mocha';
import mongoose, {Mongoose} from 'mongoose';
import config from '../config';

import {
  addOrUpdate,
  addOrUpdateMany,
  get,
  getTitlesWithIdp,
  removeByIdp,
  removeByIpdMany
} from './Products';

let connectionDb: Mongoose;

const products: IProductItem[] = [
  {
    idp: 1,
    names: ['hello', 'world'],
    title: 'hello'
  },
  {
    idp: 2,
    names: ['hello', 'world'],
    title: 'hello'
  },
  {
    idp: 3,
    names: ['hello', 'world'],
    title: 'hello'
  },
  {
    idp: 4,
    names: ['hello', 'world'],
    title: 'hello'
  },
  {
    idp: 5,
    names: ['hello', 'world'],
    title: 'hello'
  },
  {
    idp: 6,
    names: ['hello', 'world'],
    title: 'hello'
  },
  {
    idp: 7,
    names: ['hello', 'world'],
    title: 'hello'
  },
  {
    idp: 8,
    names: ['hello', 'world'],
    title: 'hello'
  },
  {
    idp: 9,
    names: ['hello', 'world'],
    title: 'hello'
  },
  {
    idp: 10,
    names: ['hello', 'world'],
    title: 'hello'
  },
  {
    idp: 11,
    names: ['hello', 'world'],
    title: 'hello'
  },
  {
    idp: 12,
    names: ['hello', 'world'],
    title: 'hello'
  }
];

describe('storages', () => {

  let connected = false;

  before(async () => {
    connectionDb = (await mongoose.connect(`mongodb://${config().mongodb.host}:${config().mongodb.port}/${config().mongodb.database}`));
    connected = true;
  });

  it('connected', () => {
    assert.equal(connected, true);
  });

  it('addOrUpdate()', async () => {
    await addOrUpdate(products[0]);
    assert.equal('', '');
  });

  it('addOrUpdateMany()', async () => {
    await addOrUpdateMany(products);
  });

  it('get()', async () => {
    const items = (await get(products.length))
      .map((item) => {
        return {
          idp: item.idp,
          names: item.names,
          title: item.title
        };
      })
      .sort((a, b) => a.idp > b.idp ? 1 : -1);
    assert.deepEqual(items, products);
  });

  it('getTitlesWithIdp()', async () => {
    const filteredProducts = products.map((item) => {
      return {
        idp: item.idp,
        title: item.title
      }
    });
    const items = (await getTitlesWithIdp(products.length))
      .map((item) => {
        return {
          idp: item.idp,
          title: item.title
        }
      })
      .sort((a, b) => a.idp > b.idp ? 1 : -1);
    assert.deepEqual(items, filteredProducts);
  });

  it('removeByIdp()', async () => {
    const filteredProducts = products.filter((item) => item.idp !== 1);
    await removeByIdp(1);
    const items = (await get(filteredProducts.length))
      .map((item) => {
        return {
          idp: item.idp,
          names: item.names,
          title: item.title
        };
      })
      .sort((a, b) => a.idp > b.idp ? 1 : -1);
    assert.deepEqual(items, filteredProducts);
  });

  it('removeByIpdMany()', async () => {
    const filteredProducts = products.filter((item) => item.idp !== 1 && item.idp !== 2 && item.idp !== 3);
    await removeByIpdMany([1, 2, 3]);
    const items = (await get(filteredProducts.length))
      .map((item) => {
        return {
          idp: item.idp,
          names: item.names,
          title: item.title
        };
      })
      .sort((a, b) => a.idp > b.idp ? 1 : -1);
    assert.deepEqual(items, filteredProducts);
  });

  after(() => {
    mongoose.connection.close();
  });
});
