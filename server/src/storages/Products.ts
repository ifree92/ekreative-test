import ProductModel from '../models/Product';

/**
 * Add item or update if not exists
 * @param {IProductItem} product
 * @returns {Promise<void>}
 */
export async function addOrUpdate(product: IProductItem): Promise<void> {
  await ProductModel.findOneAndUpdate({idp: product.idp}, product, {upsert: true}).exec();
}

/**
 * Add many items or update if not exists
 * @param {IProductItem[]} products
 * @returns {Promise<void>}
 */
export async function addOrUpdateMany(products: IProductItem[]): Promise<void> {
  for (let i = 0; i < products.length; i++) {
    const product = products[i];
    await addOrUpdate(product);
  }
}

/**
 * Get list of products with ipd and title fields only
 * @param {number} limit default: 100
 * @param {number} skip default: 0
 * @returns {Promise<IProductTitleItem>}
 */
export async function getTitlesWithIdp(limit = 100, skip = 0): Promise<IProductTitleItem[]> {
  return (await ProductModel.find({}).select('idp title').skip(skip).limit(limit).exec()).map((item) => {
    return {
      idp: item.idp,
      title: item.title
    };
  });
}

/**
 * Get list of products
 * @param {number} limit default: 100
 * @param {number} skip default: 0
 * @returns {Promise<IProductTitleItem>}
 */
export async function get(limit = 100, skip = 0): Promise<IProductItem[]> {
  return (await ProductModel.find({}).skip(skip).limit(limit).exec()).map((item) => {
    return {
      idp: item.idp,
      title: item.title,
      names: item.names
    }
  });
}

/**
 * Remove entry by idp
 * @param {number} idp
 * @returns {Promise<void>}
 */
export async function removeByIdp(idp: number): Promise<void> {
  await ProductModel.remove({idp});
}

/**
 * Remove entries by idp
 * @param {number[]} idp
 * @returns {Promise<void>}
 */
export async function removeByIpdMany(idp: number[]): Promise<void> {
  for (let i = 0; i < idp.length; i++) {
    await removeByIdp(idp[i]);
  }
}
